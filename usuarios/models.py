from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from django.conf import settings
from django.dispatch import receiver
from django.db.models.signals import post_save
from Process.models import Process
# Create your models here.

# Roles y sus Permisos
class Role(models.Model):
    code = models.IntegerField(unique=True)
    name = models.CharField(max_length=100, unique=True)
    create = models.BooleanField(default=False)
    delete = models.BooleanField(default=False)
    update = models.BooleanField(default=False)
    view = models.BooleanField(default=False)
    status = models.BooleanField(default=True)

# Modelo ligado a usuarios para extender los datos ha almacenar
class UserDetail(models.Model):
    user = models.OneToOneField(User, on_delete= models.CASCADE)
    photo = models.CharField(max_length=50,blank=True,null=True)
    status = models.BooleanField(default=True,blank=True, null=True) 
    created = models.DateTimeField(auto_now_add=True, null=True)
    last_updated = models.DateTimeField(auto_now_add=True, null=True) 

# Modelo que relaciona un usaurio un rol y un proceso para manejar los usaurios por proceso
class UserProcess(models.Model):
    user = models.ForeignKey(User, on_delete = models.CASCADE)
    role = models.ForeignKey(Role, on_delete = models.CASCADE)
    process = models.ForeignKey(Process, on_delete = models.CASCADE)
    status = models.BooleanField(default=True)

# Funciones que generan automaticamnete la creacion del detalle del usaurio al crear un nuevo usuario
@receiver(post_save, sender = User)
def create_user_detail(sender, instance, created, **kwargs):
    if created:
        UserDetail.objects.create(user = instance)
@receiver(post_save, sender = User)
def save_user_detail(sender, instance, created, **kwargs):
    instance.userdetail.save()


