from .views import *
from django.urls import path

"""
    En urlpatterns podemos encontrar todas las Urls de los servicios del API que seran consumidas por el frontend
    estas url estan agrupadas por los distintos modulos de usaurios y roles que ofrece el sistema
"""
urlpatterns = [
    path('user/set/insert/', UserDetailCreateAPIView.as_view(), name="insert_user"),
    path('user/set/register/', UserRegisterAPIView.as_view(), name="insert_user"),
    path('user/set/update/', UserDetailUpdateAPIView.as_view(), name="update_user"),
    path('user/get/select/', UserGetDataAPIView.as_view(), name="select_user"),
    path('user/find/', UserFindAPIView.as_view(), name="selectpk_user"),

    path('role/set/insert/', RoleCreate.as_view(), name="role_create"),
    path('role/get/select/', RoleGetSelect.as_view(), name="role_get"),
    path('role/get/select/<int:pk>/', RoleUpdate.as_view(), name="role_get_select"),
    path('role/set/update/<int:pk>/', RoleUpdate.as_view(), name="role_update"),
    path('role/set/delete/<int:pk>/', RoleUpdate.as_view(), name="role_delete"),

    path('user_process/set/insert/', UserProcessCreate.as_view(), name="insert_user_process"),
    path('user_process/get/select/', UserProcessCreate.as_view(), name="select_user_process"),
    path('user_process/get/select/<int:pk>/', UserProcessUpdate.as_view(), name="select_user_process"),
    path('user_process/set/update/<int:pk>/', UserProcessUpdate.as_view(), name="select_user_process"),
    path('user_process/set/delete/<int:pk>/', UserProcessUpdate.as_view(), name="select_user_process"),
]