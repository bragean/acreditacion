from rest_framework import serializers
from .models import *

# Serializador de los datos de un Usuario
class UserDetailSerializer(serializers.Serializer):
    first_name = serializers.CharField(max_length=50)
    last_name = serializers.CharField(max_length=50)
    username = serializers.CharField(max_length=25)
    photo = serializers.CharField(max_length=50)
    id_usertype = serializers.CharField()
    user_type = serializers.CharField(max_length=50)
    status =  serializers.CharField(max_length=10)

# Serializador de los Roles del Sistema
class RoleSelectSerializer(serializers.Serializer):
    code = serializers.IntegerField()
    name = serializers.CharField(max_length=50)
    status = serializers.BooleanField()

class RoleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Role
        fields = "__all__"
        
# Serializador de los usaurios  de un Proceso
class UserProcessSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProcess
        fields = "__all__"