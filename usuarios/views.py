import re
from django.shortcuts import render
from .models import *
from Process.models import Process
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializer import *
from datetime import *
from django.contrib.auth.models import User, Group
from django.contrib.auth import authenticate
from django.contrib import auth
import json
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK
)
from rest_framework_jwt.views import verify_jwt_token
from rest_framework_jwt.settings import api_settings
from rest_framework.permissions import AllowAny
from django.http import HttpResponse, JsonResponse


# Funcion que obtiene el onjeto User mediante el Token recibido del request
def get_user_by_token(request):
	try:
		token = request.META.get('HTTP_AUTHORIZATION').split(" ")[1]
		jwt_decode_handler = api_settings.JWT_DECODE_HANDLER
		jwt_get_username_from_payload = api_settings.JWT_PAYLOAD_GET_USERNAME_HANDLER
		payload = jwt_decode_handler(token)
		username = jwt_get_username_from_payload(payload)
		return User.objects.get(username=username)
	except:
		return None

# Funcion para validar que los id enviados por el frontend existan y pertenescan al Modelo establecido
def id_is_vaid(id, Model):
	if id == None or id == "" or Model.objects.filter(id = id).exists() == False:
		return False
	else:
		return True

# Vista para crear un Usaurio 
class UserDetailCreateAPIView(APIView):
	def post(self, request, format=None):
		user = get_user_by_token(request)

		first_name = request.data.get('first_name')
		last_name = request.data.get('last_name')
		username = request.data.get('username')
		password = request.data.get('password')
		photo = request.data.get('photo')
		status = request.data.get('status')	
		if username == None or password == None:
			print("no se puede registrar")
			return Response({'response':'failed'})
		else:
			user = User.objects.create_user(username=username,password=password,first_name=first_name,last_name=last_name)
			user.set_password(password)
			user.save()
			user_detail = UserDetail.objects.get(user=user)
			user_detail.photo= photo
			user_detail.status=status
			user_detail.save()
			return Response({'response':'succes'})

# Vista Publica para registrarse en el Sistema
@permission_classes((AllowAny, ))
class UserRegisterAPIView(APIView):
	def post(self, request, format=None):
		username = request.data.get('username')
		password = request.data.get('password')
		first_name = request.data.get('first_name')
		last_name = request.data.get('last_name')

		if User.objects.filter(username=username).exists():
			return HttpResponse({'response':'El correo electronico ya está registrado'}, status=406)
		else:
			user = User.objects.create_user(username=username,password=password,first_name=first_name,last_name=last_name)
			user.set_password(password)
			user.save()
			user_detail = UserDetail.objects.get(user=user)
			user_detail.photo= ""
			user_detail.status= True
			user_detail.save()
			return Response({'response':'succes'})

# Vista para actualizar los datos del Usaurio 
class UserDetailUpdateAPIView(APIView):
	def put(self, request, format=None):
		user = get_user_by_token(request)

		first_name = request.data.get('first_name')
		last_name = request.data.get('last_name')
		photo = request.data.get('photo')

		user.first_name = first_name
		user.last_name = last_name
		user.save()

		user_detail= UserDetail.objects.get(user=user)
		user_detail.photo = photo
		user_detail.save()
		return Response({'response':'succes'})

class UserDetailActiveAPIView(RetrieveUpdateDestroyAPIView):
	serializer_class = UserDetailUpdateAPIView
	queryset = UserDetail.objects.all().order_by('created')

# Vista para buscar si existe un usuario con el mismo Username
class UserFindAPIView(APIView):
	def post(self, request, format=None):
		user = get_user_by_token(request)

		name = request.data.get("username")
		if not User.objects.filter(username = name).exists():
			return Response({'response':'user not exist'})
		elif  name == user.username:
			return Response({'response':'its you'})
		else:
			return Response({'response':'succes'})

# Vista para obtener los datos del usaurio activo en el sistema
class UserGetDataAPIView(APIView):
	def get(self, request, format=None):
		user = get_user_by_token(request)
		if user == None:
			return Response({'response':'failed'})
		user_detail = UserDetail.objects.get(user = user)
		first_name = user.first_name
		last_name = user.last_name
		photo = user_detail.photo
		status = user_detail.status
		serializer = UserDetailSerializer(data={'first_name':first_name,'last_name':last_name,'username':user.username,'photo':photo,'status':status})
		serializer.is_valid()
		return Response(serializer.data)

# Vista para crear un nuevo rol en el sistema
class RoleCreate(ListCreateAPIView):
	serializer_class = RoleSerializer
	def get_queryset(self):
		return Role.objects.all()

# Vista para obtener los roles del sistema
class RoleGetSelect(APIView):
	def get (self, request, format=None):
		roles = Role.objects.all().exclude(code=1)
		data = []
		for r in roles:
			serializer = RoleSelectSerializer(data={'code':r.code,'name':r.name,'status':r.status})
			serializer.is_valid()
			data.append(serializer.data)
		return Response(data)

# Vista para actualizar un rol del sistema
class RoleUpdate(RetrieveUpdateDestroyAPIView):
	serializer_class = RoleSerializer
	queryset = Role.objects.all()

# Vista para crear un usaurio y ligarlo a un rol y un proceso especifico
class UserProcessCreate(ListCreateAPIView):
	serializer_class = UserProcessSerializer
	def get_queryset(self):
		return UserProcess.objects.all()
# Vista para actualizar los dats de un usaurio perteneciente a un proceso
class UserProcessUpdate(RetrieveUpdateDestroyAPIView):
	serializer_class = UserProcessSerializer
	queryset = UserProcess.objects.all()