from django.contrib import admin
from django.urls import path , include , re_path
from django.conf.urls import url
from rest_framework.authtoken import views
from django.conf import settings
from django.conf.urls.static import static
from rest_framework_jwt.views import obtain_jwt_token
from usuarios.views import UserRegisterAPIView

api_urls = [
    re_path(r'^', include('usuarios.urls')),
    re_path(r'^', include('Process.urls')),
]
urlpatterns = [
    path('admin/', admin.site.urls),
    re_path(r'^api/', include(api_urls)),
    path('auth/login/',obtain_jwt_token),
    path('register/', UserRegisterAPIView.as_view(), name="insert_user"),

] 
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)