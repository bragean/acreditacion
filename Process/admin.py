from django.contrib import admin
from .models import *
# Register your models here.
admin.site.register(Process)
admin.site.register(Phase)
admin.site.register(Task)
admin.site.register(TaskDetail)
admin.site.register(TaskDocuments)