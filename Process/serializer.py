from rest_framework import serializers
from .models import *

# Serializados de Documentos
class DocumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Document
        fields = '__all__'

class DocumentActiveSerializer(serializers.ModelSerializer):
    class Meta:
        model = Document
        fields = ("status","last_updated",)

# Serializados de Procesos
class ProcessSerializer(serializers.ModelSerializer):
    class Meta:
        model = Process
        fields = '__all__'

class ProcessActiveSerializer(serializers.ModelSerializer):
    class Meta:
        model = Process
        fields = ("status","last_updated",)

# Serializador de los datos pafa el Tablero de Control sobre el Proceso
class ProcessCalculateSerializer(serializers.Serializer):
    id_phase = serializers.CharField(max_length=10)
    name = serializers.CharField(max_length=50)
    total_task = serializers.CharField(max_length=10)
    completed_task = serializers.CharField(max_length=10)
    percentage = serializers.CharField(max_length=10)

# Serializador con todos los datos de un Proceso
class ProcessAllSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField(max_length=50)
    owner = serializers.CharField(max_length=10)
    description = serializers.CharField(max_length=100)
    status_process = serializers.IntegerField()
    user_list = serializers.CharField()
    created = serializers.CharField(max_length=20)
    last_updated = serializers.CharField(max_length=20)
    status = serializers.BooleanField()

# Serializadores de Fases
class PhaseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Phase
        fields = '__all__'

class PhaseActiveSerializer(serializers.ModelSerializer):
    class Meta:
        model = Phase
        fields = ("status","last_updated",)

# Serializador de Criterios
class CriteriaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Criteria
        fields = '__all__'

class CriteriaActiveSerializer(serializers.ModelSerializer):
    class Meta:
        model = Criteria
        fields = ("status","last_updated",)

# Serializador Indicador 
class IndicadorAllSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    id_criteria= serializers.IntegerField()
    name = serializers.CharField(max_length=50)
    description = serializers.CharField(max_length=50)
    status = serializers.BooleanField()

class IndicatorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Indicator
        fields = '__all__'

class IndicatorActiveSerializer(serializers.ModelSerializer):
    class Meta:
        model = Indicator
        fields = ("status","last_updated",)

# Serializados de Tareas
class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = '__all__'

class TaskActiveSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = ("status","last_updated",)

# Serializador de todos los campos de Tareas
class TaskAllSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField(max_length=50)
    id_indicator = serializers.CharField(max_length=10)
    name_indicator = serializers.CharField(max_length=50)
    id_criteria = serializers.CharField(max_length=10)
    name_criteria = serializers.CharField(max_length=50)
    id_phase = serializers.CharField(max_length=10)
    description =serializers.CharField(max_length=500)
    end_date = serializers.DateTimeField()
    close_date = serializers.CharField(max_length=100)
    status_task = serializers.IntegerField()
    created = serializers.DateTimeField()
    user_list = serializers.CharField()
    status =serializers.CharField(max_length=1)

# Serializador de todos los campos de Tareas que pertencen al Owner de la Tarea
class TaskOwnerSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField(max_length=50)
    id_indicator = serializers.CharField(max_length=10)
    name_indicator = serializers.CharField(max_length=50)
    id_criteria = serializers.CharField(max_length=10)
    name_criteria = serializers.CharField(max_length=50)
    id_phase = serializers.CharField(max_length=10)
    description =serializers.CharField(max_length=500)
    end_date = serializers.DateTimeField()
    close_date = serializers.CharField(max_length=100)
    status_task = serializers.IntegerField()
    created = serializers.DateTimeField()
    user_list = serializers.CharField()
    owner = serializers.CharField(max_length=100)
    status_delivery = serializers.IntegerField()
    status_review = serializers.IntegerField()
    status =serializers.CharField(max_length=1)

# Serializador del detalle de las Tareas
class TaskDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = TaskDetail
        fields = "__all__"

class TaskDocumentsSerializer(serializers.ModelSerializer):
    class Meta:
        model = TaskDocuments
        fields = "__all__"

# usuario de un Proceso
class UserRoleSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=100)
    code = serializers.IntegerField()
    
# Serializados de Criterios
class CriteriaDataSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=10)
    percentage = serializers.CharField(max_length=10)

# Serializados de Combo Box
class SSelectSerializer(serializers.Serializer):
    id = serializers.CharField(max_length=10)
    name = serializers.CharField(max_length=50)

# documents
class FileSerializer(serializers.ModelSerializer):
    class Meta:
        model = File
        fields = "__all__"