from django.db import models
from django.contrib.auth.models import User

# Modelo de Documentos
class Document(models.Model):
    file = models.FileField(blank=True, null=True)
    remark = models.CharField(max_length=20, blank=True, null=True)
    last_updated = models.DateTimeField(auto_now_add=True, null=True) 

# Modelo de Procesos
class Process(models.Model):
    name = models.CharField(max_length=50,blank=True, null=True)
    id_user = models.ForeignKey(User, on_delete= models.CASCADE)
    user_list = models.CharField(max_length=200,blank=True, null=True)
    documents_list = models.CharField(max_length=200,blank=True, null=True)
    description = models.CharField(max_length=100, blank=True, null=True)
    status_process = models.IntegerField(blank=True, null=True,default=0)
    status = models.BooleanField(default=True,blank=True, null=True) 
    created = models.DateTimeField(auto_now_add=True, null=True)
    last_updated = models.DateTimeField(auto_now_add=True, null=True) 

# Modelo de Fases
class Phase(models.Model):
    id_process = models.ForeignKey(Process, on_delete= models.CASCADE)
    name = models.CharField(max_length=50,blank=True, null=True)
    description = models.CharField(max_length=100, blank=True, null=True) 
    status_phase = models.IntegerField(blank=True, null=True,default=0)
    tasks_list = models.CharField(max_length=200, blank=True, null=True)
    documents_list = models.CharField(max_length=200,blank=True, null=True)
    final_report_doc = models.CharField(max_length=200, blank=True, null=True)
    continues_doc = models.CharField(max_length=200, blank=True, null=True)
    finished = models.CharField(max_length=50, blank=True, null=True)
    status = models.BooleanField(default=True,blank=True, null=True) 
    created = models.DateTimeField(auto_now_add=True, null=True)
    last_updated = models.DateTimeField(auto_now_add=True, null=True) 

# Modelo de Critterios
class Criteria(models.Model):
    id_process = models.ForeignKey(Process, on_delete= models.CASCADE)
    name = models.CharField(max_length=50,blank=True, null=True)
    description = models.CharField(max_length=100, blank=True, null=True)
    documents_list = models.CharField(max_length=200,blank=True, null=True)
    status = models.BooleanField(default=True,blank=True, null=True)  
    created = models.DateTimeField(auto_now_add=True, null=True)
    last_updated = models.DateTimeField(auto_now_add=True, null=True) 

# Modelo de Indicadores
class Indicator(models.Model):
    id_criteria = models.ForeignKey(Criteria, on_delete= models.CASCADE)
    name = models.CharField(max_length=50,blank=True, null=True)
    description = models.CharField(max_length=100, blank=True, null=True)
    status = models.BooleanField(default=True,blank=True, null=True) 
    created = models.DateTimeField(auto_now_add=True, null=True)
    last_updated = models.DateTimeField(auto_now_add=True, null=True) 

# Modelo de Tareas
class Task(models.Model):
    id_indicator = models.ForeignKey(Indicator, on_delete= models.CASCADE)
    id_phase = models.ForeignKey(Phase, on_delete= models.CASCADE)
    name = models.CharField(max_length=50,blank=True, null=True)
    description = models.CharField(max_length=100, blank=True, null=True)
    end_date = models.DateTimeField(max_length=25, blank=True, null=True)
    id_owner = models.ForeignKey(User, on_delete= models.CASCADE)
    user_list = models.CharField(max_length=200,blank=True, null=True)
    close_date = models.CharField(max_length=25, blank=True, null=True)
    status_delivery = models.IntegerField(blank=True, null=True,default=0)
    status_review = models.IntegerField(blank=True, null=True,default=0)
    status_task = models.IntegerField(blank=True, null=True,default=0)
    status = models.BooleanField(default=True,blank=True, null=True) 
    created = models.DateTimeField(auto_now_add=True, null=True)
    last_updated = models.DateTimeField(auto_now_add=True, null=True)

# Modelo del detalle de Tareas
class TaskDetail(models.Model):
    id_task = models.ForeignKey(Task, on_delete= models.CASCADE)
    file_format = models.FileField(blank=True, null=True)
    name = models.CharField(max_length = 100, blank=True, null=True)
    file_format_full = models.FileField(blank=True, null=True)
    comment = models.CharField(max_length=250,blank=True,null=True)
    created = models.DateTimeField(auto_now_add=True, null=True)
    last_updated = models.DateTimeField(auto_now_add=True, null=True) 

# Modelo de los documentos ligados a las Tareas
class TaskDocuments(models.Model):
    id_task = models.ForeignKey(Task, on_delete = models.CASCADE)
    document = models.FileField(blank=True, null=True)
    name = models.CharField(max_length=50,blank=True, null=True)
    status_documet = models.IntegerField(blank=True, null=True)
    comment = models.CharField(max_length=100,blank=True, null=True)
    
# Modelo de Archivos 
class File(models.Model):
    file = models.FileField(blank=True, null=True)
    def __str__(self):
        return self.file.name