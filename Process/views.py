import json
from django.shortcuts import render
from .models import *
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializer import *
from datetime import *
from django.contrib.auth.models import User
from rest_framework.parsers import MultiPartParser, FormParser, FileUploadParser
from rest_framework import status
from usuarios.models import *
from datetime import datetime
import dateutil.parser
from django.utils.dateparse import parse_datetime
# importtaciones para la exportacion de documentos PDF
from django.http import HttpResponse, JsonResponse
from fpdf import FPDF
import pandas as pd
import matplotlib
from pylab import title, figure, xlabel, ylabel, xticks, bar, axis, legend, savefig

# Importaciones para la lectura de documentos excel
import openpyxl
from statistics import mean, median, mode, stdev
from rest_framework_jwt.views import verify_jwt_token
from rest_framework_jwt.settings import api_settings
# Importaciones para el manejo de los permisos 
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny


# Funcion que obtiene el onjeto User mediante el Token recibido del request
def get_user_by_token(request):
	try:
		token = request.META.get('HTTP_AUTHORIZATION').split(" ")[1]
		jwt_decode_handler = api_settings.JWT_DECODE_HANDLER
		jwt_get_username_from_payload = api_settings.JWT_PAYLOAD_GET_USERNAME_HANDLER
		payload = jwt_decode_handler(token)
		username = jwt_get_username_from_payload(payload)
		print(username)
		return User.objects.get(username=username)
	except:
		return None

# Funcion para validar que los id enviados por el frontend existan y pertenescan al Modelo establecido
def id_is_vaid(id, Model):
	if id == None or id == "" or Model.objects.filter(id = id).exists() == False:
		return False
	else:
		return True

# Clase para visualizar  los archivos
class FileView(APIView):
  parser_classes = (MultiPartParser, FormParser)
  def post(self, request, *args, **kwargs):
    file_serializer = FileSerializer(data=request.data)
    if file_serializer.is_valid():
      file_serializer.save()
      return Response(file_serializer.data, status=status.HTTP_201_CREATED)
    else:
      return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# Clase para el procesamiento de los documentos excel
class ProcessingActiveAPIView(ListCreateAPIView):
	def get(self,request,format=None):
		excel_file = request.FILES["excel_file"]
		wb = openpyxl.load_workbook(excel_file, data_only=True)
		worksheet = wb["Hoja1"]
		print(worksheet)
		excel_data = list()
		proms = []
		countRow = 0
		for row in worksheet.iter_rows():
			row_data = list()
			countCol = 0
			for cell in row:
				row_data.append(str(cell.value))
				if(countRow > 5):	
					if(countCol == 9):
						proms.append(int(cell.value))
					countCol+=1
			excel_data.append(row_data)
			countRow+=1
		#print(excel_data)
		print(proms)
		promedio = mean(proms)
		mediana = median(proms)
		stdeviation = stdev(proms)
		return Response({"response":"Success","promedio":promedio,"mediana":mediana,"stdeviation":stdeviation})			

# Clases para el CRUD de docuemntos 
class DocumentCreateAPIView(ListCreateAPIView):
	serializer_class = DocumentSerializer
	def get_queryset(self):
		return Document.objects.all().order_by('created')

class DocumentUpdateAPIView(RetrieveUpdateDestroyAPIView):
	serializer_class = DocumentSerializer
	queryset = Document.objects.all().order_by('created')

class DocumentActiveAPIView(RetrieveUpdateDestroyAPIView):
	serializer_class = DocumentUpdateAPIView
	queryset = Document.objects.all().order_by('created')

# Clase para la creacion de un nuevo proceso y obtencion de la lista de todos los procesos creados
class ProcessCreateAPIView(ListCreateAPIView):
	serializer_class = ProcessSerializer
	# mediante metodo POST se crea un nuevo Proceso
	def post(self,request, format=None):
		user = get_user_by_token(request)

		name = request.data.get("name")
		user_list = request.data.get("user_list")
		documents_list = request.data.get("document_list")
		description = request.data.get("description")
		
		process = Process.objects.create(name=name, id_user=user, user_list=user_list, documents_list=documents_list, description=description, status= True)
		process.save()

		owner = Role.objects.get(name="Propietario")
		usuario = Role.objects.get(name="Usuario")
		user_process = UserProcess.objects.create(user=user, role=owner, process = process, status=True)
		user_process.save()
		
		if user_list != None and user_list!="" :
			users = user_list.split(",")
			print(users)
			for u in users:
				user_ = User.objects.get(username=u)
				new_user_process = UserProcess.objects.create(user=user_, role=usuario, process = process, status=True)
				new_user_process.save()
		return Response({'response':'succes'})

	# mediante metodo GET se obtiene la lista de todos los Procesos Creados (Debe ser usado solo para desarrollo)
	def get(self,request,format=None):
		user = get_user_by_token(request)
		process = Process.objects.all()
		data = []
		for p in process:
			lists = p.user_list
			users = lists.split(",")
			owner = User.objects.get(username=p.id_user).username
			users.append(owner)
			for u in users:
				if u == user.username:
					owner = (p.id_user).username
					serializer = ProcessAllSerializer(data={'id':p.id,'name':p.name,'owner':owner,'description':p.description,'status_process':p.status_process,'user_list':p.user_list,'created':p.created,'last_updated':p.last_updated,'status':p.status})
					serializer.is_valid()
					data.append(serializer.data)
		return Response(data)

# Clase mediante metodo GET y el ID = pk del proceso devuleve todos sus datos
class ProcessGetAPIView(APIView):
	def get(self,request,pk,format=None):
		user = get_user_by_token(request)
		try:
			process = Process.objects.get(id = pk)
			serializer = ProcessSerializer(process)
		except:
			return Response({'response':'failed in Get'})
		return Response(serializer.data)

# Clase mediante metodo PUT y el ID = pk realiza un update del proceso
class ProcessUpdateAPIView(APIView):
	def put(self, request, pk, format=None):
		user = get_user_by_token(request)
		if not id_is_vaid(pk,Process):
			return Response({"response":"the id is invalid"})
		try:
			name = request.data.get("name")
			documents_list = request.data.get("documents_list")
			description = request.data.get("description")
			status = bool(request.data.get("status"))

			process = Process.objects.get(id = pk)
			process.name= name
			process.documents_list = documents_list
			process.description = description
			process.status= status
			process.save()
		except:
			return HttpResponse(status=400)
		return Response({'response':'succes'})

# Clase mediante metodo DELETE y el ID = pk elimina el proceso 
class ProcessDeleteAPIView(APIView):
	def delete(self, request, pk, format=None):
		user = get_user_by_token(request)
		try:
			proces = Process.objects.get(id = pk)
			proces.delete()
		except:
			return Response({'response':'failed delete'})
		return Response({'response':'succes'})

# Clase mediante metodo GET y el ID = pk del proceso cambia el estado del Proceso
class ProcessActiveAPIView(RetrieveUpdateDestroyAPIView):
	def put(self, request, pk, format=None):
		user = get_user_by_token(request)
		status = request.data.get("status")
		
		try:
			process = Process.objects.get(id = pk)
			process.status = status
			process.save()
		except:
			return Response({'response':'failed in change status'})
		return Response({'response':'succes'})

# Vistas para Calcular avance del proceso 
class ProcessCalculate(APIView):
	def get(self,request,format=None):
		phases  = Phase.objects.filter(status=True)
		data = []
		for p in phases:
			serializer = ProcessCalculateSerializer(data={'id_phase':p.id,'name':p.name,'total_task':0,'completed_task':0,'percentage':p.status_phase})
			serializer.is_valid()
			data.append(serializer.data)
		return Response(data)

# Clase que devuelve todos los procesos que pertenecen a un usuario
class ProcessSelectUser(APIView):
	def get(self, request, pk ,format=None):
		process = Process.objects.get(id = pk)
		user_process = UserProcess.objects.filter(process=process)
		data=[]
		for process in user_process:
			serializer = UserRoleSerializer(data={'username':(process.user).username,'code':(process.role).code})
			serializer.is_valid()
			data.append(serializer.data)
		return Response(data)

# Clase para cambiar a los usuarios que pertencen a un proceso 
class ProcessChangeUser(APIView):
	def put(self, request ,format=None):
		try:
			id_process = request.data.get("id_process")
			username = request.data.get("username")
			code = request.data.get("code")
			
			process = Process.objects.get(id = id_process)
			user = User.objects.get(username= username)
			role = Role.objects.get(code=code)
			user_process = UserProcess.objects.get(user=user,process=process)
			user_process.role=role
			user_process.save()
			return HttpResponse(status=200)
		except:
			return HttpResponse(status=400)

# Clase que elimina a un usaurio de un proceso
class ProcessDeleteUser(APIView):
	def post(self, request ,format=None):
		try:
			id_process = request.data.get("id_process")
			username = request.data.get("username")
			
			process = Process.objects.get(id = id_process)
			user = User.objects.get(username= username)
			user_process = UserProcess.objects.get(user=user,process=process)
			user_process.delete()

			users = process.user_list.split(",")
			users.remove(username)
			process.user_list= ','.join(users)
			process.save()
			return HttpResponse(status=200)
		except:
			return HttpResponse(status=400)

# Clase que añade un nuevo usuario en el proceso
class ProcessInsertUser(APIView):
	def post(self, request ,format=None):
		try:
			id_process = request.data.get("id_process")
			username = request.data.get("username")

			process = Process.objects.get(id = id_process)
			user = User.objects.get(username= username)
			role = Role.objects.get(name="Usuario")
			user_process = UserProcess.objects.create(user=user,process=process,role=role,status=True)
			user_process.save()

			users = process.user_list.split(",")
			users.append(username)
			users = ','.join(users)
			if  users[0]==',':
				users = users[1:]
			print(users)
			process.user_list=  users
			process.save()
			return HttpResponse(status=200)
		except:
			return HttpResponse(status=400)

# Clase devuelve los procesos del usaurio actual
class ProcessSelect(APIView):
	def get(self,request,format=None):
		proceces = Process.objects.all()
		data = []
		for p in proceces:
			try:
				name_user = User.objects.get(id=p.id_user).first_name
			except:
				name_user = ""
			serializer = ProcessAllSerializer(data={'id':p.id,'name':p.name,'id_user':p.id_user,'name_user':name_user,'user_list':p.user_list,'documents_list':p.documents_list,'created':p.created,'last_updated':p.last_updated,'description':p.description,'status_process':p.status_process,'status':p.status})
			serializer.is_valid()
			data.append(serializer.data)
		return Response(data)

# Vistas para crear una Fases
class PhaseCreateAPIView(ListCreateAPIView):
	serializer_class = PhaseSerializer
	def get_queryset(self):
		return Phase.objects.all().order_by('created')
	def post(self,request, format=None):
		user = get_user_by_token(request)
		id_process = request.data.get("id_process")
		if not id_is_vaid(id_process,Process):
			return Response({"response":"the id is invalid"})
		name = request.data.get("name")
		description = request.data.get("description")
		process = Process.objects.get(id=id_process)
		try:
			phase = Phase.objects.create(id_process=process,name=name,description=description, status_phase= 0, status= True)
			phase.save()
		except:
			return HttpResponse(status=400)
		return Response({'response':'succes'})

# Vistas para actualizar una Fases
class PhaseUpdateAPIView(APIView):
	def put(self,request, pk, format=None):
		user = get_user_by_token(request)
		if not id_is_vaid(pk,Phase):
			return Response({"response":"the id is invalid"})
		try: 
			phase = Phase.objects.get(id=pk)
			phase.name = request.data.get("name")
			phase.description = request.data.get("description")
			phase.finished = request.data.get("finished")
			phase.status = bool(request.data.get("status"))
			phase.save()
		except:
			return HttpResponse(status=400)
		return Response({'response':'succes'})
	def get(self,request, pk, format=None):
		user = get_user_by_token(request)
		if not id_is_vaid(pk,Phase):
			return Response({"response":"the id is invalid"})
		try: 
			phase = Phase.objects.get(id=pk)
			serializer = PhaseSerializer(phase)
			return Response(serializer.data)
		except:
			return HttpResponse(status=400)
		return Response({'response':'succes'})

# Vistas para eliminar una Fases
class PhaseDeleteAPIView(APIView):
	def delete(self,request, pk, format=None):
		user = get_user_by_token(request)
		if not id_is_vaid(pk,Phase):
			return Response({"response":"the id is invalid"})
		phase = Phase.objects.get(id=pk)
		phase.delete()
		return Response({'response':'succes'})

# Vistas para cambiar el estado de una Fases
class PhaseActiveAPIView(APIView):
	def put(self,request, pk, format=None):
		user = get_user_by_token(request)
		if not id_is_vaid(pk,Phase):
			return Response({"response":"the id is invalid"})

		phase = Phase.objects.get(id=pk)
		phase.status = bool(request.data.get("status"))
		phase.save()
		return Response({'response':'succes'})

# Vistas para obtener una Fases por el id del Proceso
class PhaseSelectProcess(APIView):
	def get(self,request,pk, format=None):
		user = get_user_by_token(request)
		if not id_is_vaid(pk,Process):
			return Response({"response":"the id is invalid"})
		phases = Phase.objects.filter(id_process=pk)
		serializer = PhaseSerializer(phases,many=True)
		return Response(serializer.data)

# Vistas para calcular el avance de una Fases
class PhaseCalculate(APIView):
	def get(self,request,pk,format=None):
		tasks = Task.objects.filter(id_phase=pk,status=True)
		total_tasks = 0
		complete_tasks = 0
		for t in tasks:
			total_tasks = total_tasks + 1 
			if t.status_task==2:
				complete_tasks = complete_tasks + 1
		if total_tasks == 0:
			percentage = 0
		else: 	
			percentage = (complete_tasks*100)/total_tasks
		phase = Phase.objects.get(id=pk)
		phase.status_phase= percentage
		phase.save()
		return Response({"total":total_tasks,"completed":complete_tasks,"percentage":percentage})

# Vistas para crear un Criterio
class CriteriaCreateAPIView(ListCreateAPIView):
	serializer_class = CriteriaSerializer
	def get_queryset(self):
		return Criteria.objects.all().order_by('created')
	def post(self,request, format=None):
		user = get_user_by_token(request)
		id_process = request.data.get("id_process")
		if not id_is_vaid(id_process,Process):
			return Response({"response":"the id is invalid"})
		name = request.data.get("name")
		description = request.data.get("description")
		process = Process.objects.get(id = id_process)
		try:
			criteria = Criteria.objects.create(id_process=process,name=name,description=description, status= True)
			criteria.save()
		except:
			return HttpResponse(status=400)
		return Response({'response':'succes'})

# Vistas para actualizar un criterio por el id
class CriteriaUpdateAPIView(RetrieveUpdateDestroyAPIView):
	serializer_class = CriteriaSerializer
	queryset = Criteria.objects.all().order_by('created')
	def post(self,request, pk, format=None):
		user = get_user_by_token(request)
		if not id_is_vaid(pk,Criteria):
			return Response({"response":"the id is invalid"})
		try:
			criteria = Criteria.objects.get(id=pk)
			criteria.name = request.data.get("name")
			criteria.description = request.data.get("description")
			criteria.status = bool(request.data.get("status"))
			criteria.save()
		except:
			return Response({'response':'failed in get Data'})
		return Response({'response':'succes'})

# Vistas para eliminar un criterio por el id
class CriteriaDeleteAPIView(APIView):
	def delete(self,request, pk, format=None):
		user = get_user_by_token(request)
		if not id_is_vaid(pk,Criteria):
			return HttpResponse(status=400)
		criteria = Criteria.objects.get(id=pk)
		criteria.delete()
		return Response({'response':'succes'})

# Vistas para cambiar el estado un criterio por el id
class CriteriaActiveAPIView(APIView):
	def put(self,request, pk, format=None):
		user = get_user_by_token(request)
		if not id_is_vaid(pk,Criteria):
			return Response({"response":"the id is invalid"})
		criteria = Criteria.objects.get(id=pk)
		criteria.status = bool(request.data.get("status"))
		criteria.save()
		return Response({'response':'succes'})

# Vistas para obetner un criterio por el id de un Proceso
class CriteriaSelectProcess(APIView):
	def get(self,request,pk, format=None):
		user = get_user_by_token(request)
		if not id_is_vaid(pk,Process):
			return Response({"response":"the id is invalid"})
		criterias = Criteria.objects.filter(id_process=pk)
		serializer = CriteriaSerializer(criterias,many=True)
		return Response(serializer.data)

# Vistas para calcular en avance un criterio por el id
class CriteriaCalculate(APIView):
	def get(self,request,pk,pk2,format=None):
		indicators = Indicator.objects.filter(id_criteria = pk2)
		data = []
		for i in indicators:
			tasks = Task.objects.filter(id_phase=pk,id_indicator = i.id ,status=True)
			name = i.name
			total_tasks = 0
			complete_tasks = 0
			for t in tasks:
				total_tasks = total_tasks + 1 
				if t.status_task== 2:
					complete_tasks = complete_tasks + 1
			if total_tasks == 0:
				total = 0
			else:
				total = (100*complete_tasks)/total_tasks
				
			serializer = CriteriaDataSerializer(data={'name':name,'percentage':total})
			serializer.is_valid()
			data.append(serializer.data)
		return Response(data)

# Vistas para crear un Indicador 
class IndicatorCreateAPIView(ListCreateAPIView):
	serializer_class = IndicatorSerializer
	def get_queryset(self):
		return Indicator.objects.all().order_by('created')
	def post(self,request, format=None):
		user = get_user_by_token(request)
		id_criteria = request.data.get("id_criteria")
		if not id_is_vaid(id_criteria,Criteria):
			return HttpResponse(status=400)
		name = request.data.get("name")
		description = request.data.get("description")
		criteria = Criteria.objects.get(id = id_criteria)
		try:
			indicator = Indicator.objects.create(id_criteria=criteria,name=name,description=description, status= True)
			indicator.save()
		except:
			return HttpResponse(status=400)
		return Response({'response':'succes'})

# Vistas para actualizar, eliminar, obtener un Indicador por el id
class IndicatorUpdateAPIView(APIView):
	def put(self,request, pk, format=None):
		user = get_user_by_token(request)
		if not id_is_vaid(pk,Indicator):
			return Response({"response":"the id is invalid"})
		try:
			indicator = Indicator.objects.get(id=pk)
			indicator.name = request.data.get("name")
			indicator.description = request.data.get("description")
			indicator.status = bool(request.data.get("status"))
			indicator.save()
		except:
			return HttpResponse(status=400)
		return Response({'response':'succes'})
	def delete(self,request, pk, format=None):
		user = get_user_by_token(request)
		if not id_is_vaid(pk,Indicator):
			return Response({"response":"the id is invalid"})
		try:
			indicator = Indicator.objects.get(id=pk)
			indicator.delete()
		except:
			return HttpResponse(status=400)
		return Response({'response':'succes'})
	def get(self,request,pk, format=None):
		user = get_user_by_token(request)
		if not id_is_vaid(pk,Indicator):
			return Response({"response":"the id is invalid"})
		indicator = Indicator.objects.get(id=pk)
		serializer = IndicatorSerializer(indicator)
		return Response(serializer.data)

# Vistas para actualizar el estado de un Indicador por el id
class IndicatorActiveAPIView(APIView):
	def put(self,request, pk, format=None):
		user = get_user_by_token(request)
		if not id_is_vaid(pk,Indicator):
			return Response({"response":"the id is invalid"})

		indicator = Indicator.objects.get(id=pk)
		indicator.status = bool(request.data.get("status"))
		indicator.save()
		return Response({'response':'succes'})

# Vistas para obtener un Indicador por el id de un Criterio
class IndicatorSelectCriteria(APIView):
	def get(self,request,pk, format=None):
		user = get_user_by_token(request)
		if not id_is_vaid(pk,Criteria):
			return Response({"response":"the id is invalid"})
		indicators = Indicator.objects.filter(id_criteria=pk)
		serializer = IndicatorSerializer(indicators,many=True)
		return Response(serializer.data)

# Vistas para obtener un Indicador por el id de un Proceso
class IndicatorSelectProcess(APIView):
	def get(self,request,pk, format=None):
		user = get_user_by_token(request)
		if not id_is_vaid(pk,Process):
			return Response({"response":"the id is invalid"})
		process = Process.objects.get(id=pk)
		criterias = Criteria.objects.filter(id_process=process)
		data = []
		for c in criterias:
			indicators = Indicator.objects.filter(id_criteria=c)
			for i in indicators:
				serializer = IndicadorAllSerializer(data = { 'id':i.id,'id_criteria':i.id_criteria.id,'name':i.name,'description':i.description,'status':i.status})
				serializer.is_valid()
				data.append(serializer.data)
		return Response(data)

# Vistas para obtener el avance de  un Indicador por el id
class IndicatorCalculate(APIView): 
	def get(self,request,pk,pk2,format=None):
		tasks = Task.objects.filter(id_phase=pk,id_indicator = pk2,status=True)
		total_tasks = 0
		complete_tasks = 0
		for t in tasks:
			total_tasks = total_tasks + 1 
			if t.status_task== 2:
				complete_tasks = complete_tasks + 1
		if total_tasks == 0:
			total = 0
		else:
			total = (100*complete_tasks)/total_tasks
		return Response({"total":total_tasks,"completed":complete_tasks,"total_percentage":total})

# Vistas para crear y obetner una Tarea 
class TaskCreateAPIView(APIView):
	def post(self,request, format=None):
		user = get_user_by_token(request)
		id_indicator = request.data.get("id_indicator")
		if not id_is_vaid(id_indicator,Indicator):
			return Response({"response":"the id is invalid"})
		id_phase = request.data.get("id_phase")
		if not id_is_vaid(id_phase,Phase):
			return Response({"response":"the id is invalid"})
		name = request.data.get("name")
		description = request.data.get("description")
		# here is user id
		user_list = request.data.get("user_list")
		end_date = request.data.get("end_date")
		indicator = Indicator.objects.get(id=id_indicator)
		phase = Phase.objects.get(id=id_phase)
		try:
			task = Task.objects.create(id_indicator=indicator,id_phase=phase,end_date=end_date,name=name,description=description,id_owner=user,user_list=user_list,status_task=0, status= True)
			task.save()
			users = user_list.split(",")
		except:
			return HttpResponse(status=400)
		return Response({'response':'succes'})

	def get(self, request, format=None):
		user = get_user_by_token(request)
		tasks = Task.objects.filter(id_owner=user)
		data = []
		for t in tasks:
			try:
				name_indicator = Indicator.objects.get(id=t.id_indicator.id).name
			except:
				name_indicator = ""
			try:
				id_criteria = Indicator.objects.get(id=t.id_indicator.id).id_criteria
			except:
				id_criteria = ""
			try:
				name_criteria = Criteria.objects.get(id=id_criteria.id).name
			except:
				name_criteria = ""
			serializer = TaskAllSerializer(data={'id':t.id,'name':t.name,'id_indicator':t.id_indicator.id,'name_indicator':name_indicator,'id_criteria':id_criteria.id,'name_criteria':name_criteria,'id_phase':t.id_phase.id,'description':t.description,'end_date':t.end_date,'close_date':t.close_date,'status_task':t.status_task,'created':t.created,'user_list':t.user_list,'status':t.status})
			serializer.is_valid()
			data.append(serializer.data)
		return Response(data)

# Vistas para actualizar una Tarea por el ID 
class TaskUpdateAPIView(APIView):
	def get(self, request, pk, format=None):
		user = get_user_by_token(request)
		if not id_is_vaid(pk,Task):
			return Response({"response":"the id is invalid"})
		t = Task.objects.get(id=pk)
		data =[]
		try:
			name_indicator = Indicator.objects.get(id=t.id_indicator.id).name
		except:
			name_indicator = ""
		try:
			id_criteria = Indicator.objects.get(id=t.id_indicator.id).id_criteria.id
		except:
			id_criteria = ""
		try:
			name_criteria = Criteria.objects.get(id=id_criteria).name
		except:
			name_criteria = ""
		serializer = TaskOwnerSerializer(data={'id':t.id,'status_delivery':t.status_delivery,'status_review':t.status_review,'name':t.name,'id_indicator':t.id_indicator.id,'name_indicator':name_indicator,'id_criteria':id_criteria,'name_criteria':name_criteria,'id_phase':t.id_phase.id,'description':t.description,'end_date':t.end_date,'close_date':t.close_date,'status_task':t.status_task,'created':t.created,'status':t.status, 'owner':t.id_owner.username})
		serializer.is_valid()
		return Response(serializer.data)

	def put(self, request, pk, format=None):
		user = get_user_by_token(request)
		try:
			task = Task.objects.get(id=pk)
			task.name = request.data.get("name")
			task.description = request.data.get("description")
			task.user_list = request.data.get("user_list")
			task.end_date = request.data.get("end_date")
			status_task = request.data.get("status_task")
			if status_task != None or status_task != "":
				task.status_task = status_task
			task.save()
		except:
			return HttpResponse(status=400)
		return Response({'response':'succes'})

# Vistas para eliminar una Tarea por el ID
class TaskDeleteAPIView(APIView):
	def delete(self,request, pk, format=None):
		user = get_user_by_token(request)
		if not id_is_vaid(pk,Task):
			return HttpResponse('id invalido',status=400)
		task = Task.objects.get(id=pk)
		if task.id_owner == user:
			task.delete()
		else:
			return HttpResponse('no es el propietario',status=400)
		return Response({'response':'succes'})

# Vistas para actualizar  el estado de una Tarea por el ID
class TaskActiveAPIView(APIView):
	def put(self,request, pk, format=None):
		user = get_user_by_token(request)
		if not id_is_vaid(pk,Task):
			return Response({"response":"the id is invalid"})
		task = Task.objects.get(id=pk)
		task.status = bool(request.data.get("status"))
		task.save()
		return Response({'response':'succes'})

# Vistas para obetner las Tarea por el ID de Indicador
class TaskSelectIndicator(APIView):
	def get(self,request,pk, format=None):
		user = get_user_by_token(request)

		if not id_is_vaid(pk,Indicator):
			return Response({"response":"the id is invalid"})
		indicators = Task.objects.filter(id_indicator=pk)
		serializer = TaskSerializer(indicators,many=True)
		return Response(serializer.data)

# Vistas para obetner las Tarea por el ID de Fase
class TaskSelectPhase(APIView):
	def get(self,request,pk, format=None):
		user = get_user_by_token(request)
		if not id_is_vaid(pk,Phase):
			return Response({"response":"the id is invalid"})
		phase = Phase.objects.get(id = pk)
		tasks = Task.objects.filter(id_owner=user, id_phase=phase)
		data = []
		for t in tasks:
			try:
				name_indicator = Indicator.objects.get(id=t.id_indicator.id).name
			except:
				name_indicator = ""
			try:
				id_criteria = Indicator.objects.get(id=t.id_indicator.id).id_criteria
			except:
				id_criteria = ""
			try:
				name_criteria = Criteria.objects.get(id=id_criteria.id).name
			except:
				name_criteria = ""
			serializer = TaskAllSerializer(data={'id':t.id,'name':t.name,'id_indicator':t.id_indicator.id,'name_indicator':name_indicator,'id_criteria':id_criteria.id,'name_criteria':name_criteria,'id_phase':t.id_phase.id,'description':t.description,'end_date':t.end_date,'close_date':t.close_date,'status_task':t.status_task,'created':t.created,'user_list':t.user_list,'status':t.status})
			serializer.is_valid()
			data.append(serializer.data)
		return Response(data)

# Vistas para obtener las Tarea que pertenecen al usuario actual
class TaskSelectUser(APIView):
	def get(self,request,pk_phase ,format=None):
		user = get_user_by_token(request)
		if not id_is_vaid(pk_phase,Phase):
			return Response({"response":"the id is invalid"})
		tasks = Task.objects.filter(id_phase = pk_phase)
		data =[]
		for t in tasks:
			if t.user_list == None or t.user_list == "":
				continue
			lista = t.user_list.split(",")
			if user.username in lista:
				try:
					name_indicator = Indicator.objects.get(id=t.id_indicator.id).name
				except:
					name_indicator = ""
				try:
					id_criteria = Indicator.objects.get(id=t.id_indicator.id).id_criteria.id
				except:
					id_criteria = ""
				try:
					name_criteria = Criteria.objects.get(id=id_criteria).name
				except:
					name_criteria = ""
				serializer = TaskOwnerSerializer(data={'id':t.id,'status_delivery':t.status_delivery,'status_review':t.status_review,'name':t.name,'id_indicator':t.id_indicator.id,'name_indicator':name_indicator,'id_criteria':id_criteria,'name_criteria':name_criteria,'id_phase':t.id_phase.id,'description':t.description,'end_date':t.end_date,'close_date':t.close_date,'status_task':t.status_task,'created':t.created,'status':t.status, 'owner':t.id_owner.username})
				serializer.is_valid()
				data.append(serializer.data)
		return Response(data)

# Vistas para obtener las Tarea que es propietario al usuario actual
class TaskSelectOwner(APIView):
	def get(self, request, pk_phase, format=None):
		user = get_user_by_token(request)
		if not id_is_vaid(pk_phase,Phase):
			return Response({"response":"the id is invalid"})
		tasks = Task.objects.filter(id_phase = pk_phase, id_owner=user)
		data =[]

		for t in tasks:
			try:
				name_indicator = Indicator.objects.get(id=t.id_indicator.id).name
			except:
				name_indicator = ""
			try:
				id_criteria = Indicator.objects.get(id=t.id_indicator.id).id_criteria.id
			except:
				id_criteria = ""
			try:
				name_criteria = Criteria.objects.get(id=id_criteria).name
			except:
				name_criteria = ""
			serializer = TaskOwnerSerializer(data={'id':t.id,'status_delivery':t.status_delivery,'status_review':t.status_review,'name':t.name,'id_indicator':t.id_indicator.id,'name_indicator':name_indicator,'id_criteria':id_criteria,'name_criteria':name_criteria,'id_phase':t.id_phase.id,'description':t.description,'end_date':t.end_date,'close_date':t.close_date,'status_task':t.status_task,'created':t.created,'status':t.status, 'owner':t.id_owner.username})
			serializer.is_valid()
			data.append(serializer.data)
		return Response(data)

# Vistas para el detalle de Tareas
@permission_classes((AllowAny, ))
class TaskDetailCreateAPIView(ListCreateAPIView):
	serializer_class = TaskDetailSerializer
	def get_queryset(self):
		return TaskDetail.objects.all().order_by('created')
@permission_classes((AllowAny, ))
class TaskDetailUpdateAPIView(RetrieveUpdateDestroyAPIView):
	serializer_class = TaskDetailSerializer
	queryset = TaskDetail.objects.all().order_by('created')
@permission_classes((AllowAny, ))
class TaskDocumentsCreateAPIView(ListCreateAPIView):
	serializer_class = TaskDocumentsSerializer
	def get_queryset(self):
		return TaskDocuments.objects.all()
@permission_classes((AllowAny, ))
class TaskDocumentsUpdateAPIView(RetrieveUpdateDestroyAPIView):
	serializer_class = TaskDocumentsSerializer
	queryset = TaskDocuments.objects.all()

# Vistas para el manejo de los documentos de la Tarea
class TaskDocumentsSelectTask(APIView):
	def get(self,request,pk, format=None):
		details = TaskDocuments.objects.filter(id_task=pk)
		serializer = TaskDocumentsSerializer(details,many=True)
		return Response(serializer.data)

class TaskDetailSelectTask(APIView):
	def get(self,request,pk, format=None):
		details = TaskDetail.objects.filter(id_task=pk)
		serializer = TaskDetailSerializer(details,many=True)
		return Response(serializer.data)

class DocumentUpdateAPIView(RetrieveUpdateDestroyAPIView):
	serializer_class = DocumentSerializer
	queryset = Document.objects.all().order_by('created')

# vista para subir docuentos
class FileUploadView(APIView):
	parser_class = (FileUploadParser,)
	def post(self, request, *args, **kwargs):
		file_serializer = FileSerializer(data=request.data)
		if file_serializer.is_valid():
			file_serializer.save()
			return Response(file_serializer.data, status=status.HTTP_201_CREATED)
		else:
			return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class PDF(FPDF):
    def header(self):
        self.set_font('Arial', 'B', 15)
        self.cell(80)
        self.cell(30, 10, 'MSAP', 1, 0, 'C')
        self.ln(40)

# Vistas para los Reportes
class ReportAPIView(APIView):

	def get(self,request, format=None):
		if request.data.get("id") == None:
			return JsonResponse({'status':'error', 'detail':'No user parameter'})
		
		id_p = int(request.data.get("id"))
		try:
			all_users = User.objects.get(id = id_p)
		except:
			return JsonResponse({'status':'error', 'detail':'id user error'})
		
		all_entries = Process.objects.filter(id_user = id_p)
		if len(all_entries) == 0 :
			return JsonResponse({'status':'error','detail':'User doesnt have any process'})
		
		numberP = []
		process_name = []
		status_pro = []
		status_not_process = []
		notation = 0

		for t in all_entries:
			numberP.append("P" + str(notation))
			process_name.append(t.name)
			if t.status_process != None:
				status_pro.append(t.status_process)
				status_not_process.append(100 - t.status_process)
			else:
				status_pro.append(0)
				status_not_process.append(0)
			notation = notation + 1

		title("Porcentaje de Cumplimiento")
		xlabel('Número del Proceso')
		ylabel('Porcentaje de Avance')

		df = pd.DataFrame({'Completado': status_pro,'Incompleto': status_not_process}, index=process_name)
		ax = df.plot.bar(rot=1)
		savefig('./media/barchart.png')
		legend()

		pdf = PDF()
		pdf.add_page()
		pdf.image("92.jpeg",10,6,30)
		pdf.set_xy(0, 40)
		pdf.set_font('arial', 'B', 12)
		pdf.cell(60)
		pdf.cell(75, 10, "Reporte General de los Procesos", 0, 2, 'C')
		pdf.cell(90, 10, " ", 0, 2, 'C')
		pdf.cell(-40)
		pdf.cell(20, 10, 'N°', 1, 0, 'C')
		pdf.cell(100, 10, 'Nombre del Proceso', 1, 0, 'C')
		pdf.cell(40, 10, 'Avance', 1, 2, 'C')
		pdf.cell(-120)
		pdf.set_font('arial', '', 12)
		notation = 0
		for x in range(len(process_name)):
			pdf.cell(20, 10, str(x), 1, 0, 'C')
			pdf.cell(100, 10, process_name[x], 1, 0, 'C')
			pdf.cell(40, 10, str(status_pro[x]), 1, 2, 'C')
			pdf.cell(-120)
			notation = notation + 1
		pdf.cell(30, 10, " ", 0, 2, 'C')
		pdf.cell(-30)
		pdf.image('./media/barchart.png', x = None, y = None, w = 0, h = 0, type = '', link = '')
		#pdf.output('test.pdf', 'I')

		filename = "sample_pdf.pdf"
		response = HttpResponse(pdf.output(dest='S').encode('latin-1'), content_type='application/pdf')
		response['Content-Disposition'] = 'filename="' + filename + '"'
		
		return response
