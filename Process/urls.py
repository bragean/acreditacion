from .views import *
from . import views
from django.urls import path

"""
    En urlpatterns podemos encontrar todas las Urls de los servicios del API que seran consumidas por el frontend
    estas url estan agrupadas por los distintos modulos que ofrece el sistema
"""
urlpatterns = [
    path('process/set/insert/', ProcessCreateAPIView.as_view(), name="insert_process"),
    path('process/set/update/<int:pk>/', ProcessUpdateAPIView.as_view(), name="update_process"),
    path('process/set/delete/<int:pk>/', ProcessDeleteAPIView.as_view(), name="delete_process"),
    path('process/set/active/<int:pk>/', ProcessActiveAPIView.as_view(), name="active_process"),
    path('process/get/select/', ProcessCreateAPIView.as_view(), name="select_process"),
    path('process/get/select/<int:pk>/', ProcessGetAPIView.as_view(), name="selectpk_process"),
    path('process/get/calculate/', ProcessCalculate.as_view(), name="phase_calculate"),
    path('process/get/select/user/<int:pk>/', ProcessSelectUser.as_view(), name="select_user"),
    path('process/set/update/user/', ProcessChangeUser.as_view(), name="select_user"),
    path('process/set/delete/user/', ProcessDeleteUser.as_view(), name="select_user"),
    path('process/set/insert/user/', ProcessInsertUser.as_view(), name="select_user"),

    path('phase/set/insert/', PhaseCreateAPIView.as_view(), name="insert_phase"),
    path('phase/set/update/<int:pk>/', PhaseUpdateAPIView.as_view(), name="update_phase"),
    path('phase/set/delete/<int:pk>/', PhaseDeleteAPIView.as_view(), name="delete_phase"),
    path('phase/set/active/<int:pk>/', PhaseActiveAPIView.as_view(), name="active_phase"),
    path('phase/get/select/', PhaseCreateAPIView.as_view(), name="select_phase"),
    path('phase/get/select/<int:pk>/', PhaseUpdateAPIView.as_view(), name="selectpk_phase"),
    path('phase/get/select/process/<int:pk>/', PhaseSelectProcess.as_view(), name="selectpk_phase"),
    path('phase/get/calculate/<int:pk>/', PhaseCalculate.as_view(), name="phase_calculate"),

    path('criteria/set/insert/', CriteriaCreateAPIView.as_view(), name="insert_criteria"),
    path('criteria/set/update/<int:pk>/', CriteriaUpdateAPIView.as_view(), name="update_criteria"),
    path('criteria/set/delete/<int:pk>/', CriteriaDeleteAPIView.as_view(), name="delete_criteria"),
    path('criteria/set/active/<int:pk>/', CriteriaActiveAPIView.as_view(), name="active_criteria"),
    path('criteria/get/select/', CriteriaCreateAPIView.as_view(), name="select_criteria"),
    path('criteria/get/select/<int:pk>/', CriteriaUpdateAPIView.as_view(), name="selectpk_criteria"),
    path('criteria/get/select/process/<int:pk>/', CriteriaSelectProcess.as_view(), name="selectpk_phase"),
    path('criteria/get/calculate/<int:pk>/<int:pk2>/', CriteriaCalculate.as_view(), name="selectpk_phase"),

    path('indicator/set/insert/', IndicatorCreateAPIView.as_view(), name="insert_indicator"),
    path('indicator/set/update/<int:pk>/', IndicatorUpdateAPIView.as_view(), name="update_indicator"),
    path('indicator/set/delete/<int:pk>/', IndicatorUpdateAPIView.as_view(), name="delete_indicator"),
    path('indicator/set/active/<int:pk>/', IndicatorActiveAPIView.as_view(), name="active_indicator"),
    path('indicator/get/select/', IndicatorCreateAPIView.as_view(), name="select_indicator"),
    path('indicator/get/select/<int:pk>/', IndicatorUpdateAPIView.as_view(), name="selectpk_indicator"),
    path('indicator/get/select/criteria/<int:pk>/', IndicatorSelectCriteria.as_view(), name="selectpk_phase"),
    path('indicator/get/select/process/<int:pk>/', IndicatorSelectProcess.as_view(), name="selectpk_phase"),
    path('indicator/get/calculate/<int:pk>/<int:pk2>/', IndicatorCalculate.as_view(), name="selectpk_phase"),

    path('task/set/insert/', TaskCreateAPIView.as_view(), name="insert_task"),
    path('task/set/update/<int:pk>/', TaskUpdateAPIView.as_view(), name="update_task"),
    path('task/set/delete/<int:pk>/', TaskDeleteAPIView.as_view(), name="delete_task"),
    path('task/set/active/<int:pk>/', TaskActiveAPIView.as_view(), name="active_task"),
    path('task/get/select/', TaskCreateAPIView.as_view(), name="select_task"),
    path('task/get/select/<int:pk>/', TaskUpdateAPIView.as_view(), name="selectpk_task"),
    path('task/get/select/indicator/<int:pk>/', TaskSelectIndicator.as_view(), name="selectpk_phase"),
    path('task/get/select/phase/<int:pk>/', TaskSelectPhase.as_view(), name="selectpk_phase"),
    path('task/get/select/user/<int:pk_phase>/', TaskSelectUser.as_view(), name="selectpk_phase"),
    path('task/get/select/owner/<int:pk_phase>/', TaskSelectOwner.as_view(), name="selectpk_phase"),
    
    path('task_detail/set/insert/', TaskDetailCreateAPIView.as_view(), name="insert_task"),
    path('task_detail/set/update/<int:pk>/', TaskDetailUpdateAPIView.as_view(), name="update_task"),
    path('task_detail/set/delete/<int:pk>/', TaskDetailUpdateAPIView.as_view(), name="delete_task"),
    path('task_detail/get/select/', TaskDetailCreateAPIView.as_view(), name="select_task"),
    path('task_detail/get/select/<int:pk>/', TaskDetailUpdateAPIView.as_view(), name="selectpk_task"),
    path('task_detail/get/select/task/<int:pk>/', TaskDetailSelectTask.as_view(), name="selectpk_task"),

    path('task_documents/set/insert/', TaskDocumentsCreateAPIView.as_view(), name="insert_task"),
    path('task_documents/set/update/<int:pk>/', TaskDocumentsUpdateAPIView.as_view(), name="update_task"),
    path('task_documents/set/delete/<int:pk>/', TaskDocumentsUpdateAPIView.as_view(), name="delete_task"),
    path('task_documents/get/select/', TaskDocumentsCreateAPIView.as_view(), name="select_task"),
    path('task_documents/get/select/<int:pk>/', TaskDocumentsUpdateAPIView.as_view(), name="selectpk_task"),
    path('task_documents/get/select/task/<int:pk>/', TaskDocumentsSelectTask.as_view(), name="selectpk_task"),

    path('report/', ReportAPIView.as_view(), name='report'),
    path('upload/', FileView.as_view(), name='file-upload'),
    path('processing/', ProcessingActiveAPIView.as_view(), name='processing-upload'),
]